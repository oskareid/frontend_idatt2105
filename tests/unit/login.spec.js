import LoginForm from "@/components/LoginForm.vue";
import { mount, shallowMount } from '@vue/test-utils';
import { createStore } from 'vuex';
import store from "@/store";



describe('LoginForm', () => {
     let dummyEmail = 'some@email.com'
     let dummypassword = 'somePassowrd'

     let mockedSetToken
     let mockedSetLogin
     let mockedSetMessage

     let mockedLoggedIn
     let mockedMessage
     let mockedToken

     let store

    const mockPost = jest.fn()

    const mockGet = jest.fn()

     jest.mock('axios', () => ({
         post: () => mockPost()

     }))

     jest.mock('axios', () => ({
        get: () => mockGet()
    }))


    beforeEach(() => {
        mockedSetToken = jest.fn()
        mockedSetLogin = jest.fn()
        mockedSetMessage = jest.fn()

        store = createStore({
            state: { loggedIn: mockedLoggedIn,
                     message: mockedMessage,
                     token: mockedToken},
            mutations: { setToken: mockedSetToken,
                         setLogin: mockedSetLogin,
                         setMessage: mockedSetMessage
                          }

        })


    })

    it('Check if loginForm renders to DOM', () => {

        const wrapper = mount(LoginForm, {
            data() {
                return {
                    error: null,
                    email: '',
                    password: '',
                    token: ''
                }
            }
        });

        expect(wrapper.exists())


    }),


    it('Check if loginForm display labels', () => {

        const wrapper = mount(LoginForm)
        expect(wrapper.html()).toContain('NTNU-Email')

    }),


    it('Invalid input --> Display error message', async () => {
        const errorMsg = 'Unknown credentials'
        const wrapper = mount(LoginForm, {
            propsData: {
                error: errorMsg
            },
            global: {
                plugins: [store],
            },
        })

        await wrapper.vm.handleLogin()
        expect(wrapper.html()).toContain(errorMsg)
        console.log(wrapper.html())
    }),

    it('Tries to authenticate when login button is clicked', async () => {



        const myData = {
          error: null,
          email: dummyEmail,
          password: dummypassword,
          token: ''
        }

        const wrapper = shallowMount(LoginForm, {
            global: { plugins: [store] },
            propsData: {
                myData
            }
        })


        try {
            wrapper.setData({error: null, email: dummyEmail, password: dummypassword, token: 'sdaf325235fds'})


            await wrapper.vm.handleLogin()



            expect(mockPost).toHaveBeenCalled()
            expect(mockGet).toHaveBeenCalled()
        } catch(e) {
            console.log(e)
        }




    })

})









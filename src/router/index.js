import { createRouter, createWebHistory } from 'vue-router'
import Login from '../views/Login.vue'
import Settings from '../views/Settings.vue'
import Subjects from '../views/Subjects.vue'
import NotFound from '../views/NotFound.vue'
import StudentQueueView from '../views/StudentQueueView.vue'
import StudentAssistantQueueView from "@/views/StudentAssistantQueueView";
import store from "@/store";

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/subjects/:tab',
    name: 'Subjects',
    component: Subjects,
    meta: {
      requiresAuth: true
    }
  },
  {
    //path: '/Subjects/Exercises/:subject_id', // dynamic path
    path: '/subjects/student/:subject_code/:semester/queue',
    name: 'StudentQueueView',
    component: StudentQueueView,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/subjects/studentassistant/:subject_code/:semester/queue',
    name: 'StudentAssistantQueueView',
    component: StudentAssistantQueueView,
    meta: {
      requiresAuth: true
    }
  },

  // Redirect any none-existing path to 404 page
  {
    path: '/:catchAll(.*)',
    name: 'NotFound',
    component: NotFound
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {

    if (store.state.token === '') {
      next({ name: 'Login' })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router

import { createStore } from 'vuex'


export default createStore({
  state: { // variables accessable from anywhere within the app
    currentView:'',  // den her
    Email: '', // den her
    currentUserDetails: null,
    userRoles: null,
    token: '',
    loggedIn: false, // den her
    forgotPassword: false,
    message: '',
    currentSubject: null,
    subjects: [],
    currentOption: 'Subjects'
  },

  mutations: { // methods to change variables within the state
    setEmail(state, newEmail) {
      state.currentEmail = newEmail
    },

    setCurrentSubject(state, subject) {
      state.currentSubject = subject
    },

    setToken(state, newToken) {
      state.token = newToken
    },
    setLogin(state, newEmail) {
      state.Email = newEmail
      if(state.token == '') {
        state.loggedIn = false
      }
      else {
        state.loggedIn = true
      }
    },
    setUserAccountDetails(state, accDetails) {
      state.currentUserDetails = accDetails;
    },
    setActiveRoles(state, roles) {
      state.userRoles = roles;
    },

    forgotPassword(state) {
      state.forgotPassword = !state.forgotPassword
    },

    setMessage(state, newMessage) {
      state.message = newMessage
    },

    setSubjects(state, subjects) {
      state.subjects = subjects
    },

    setCurrentOption(state, option) {
      state.currentOption = option
    }

  },



  modules: { // to split the store into modules
  }
})

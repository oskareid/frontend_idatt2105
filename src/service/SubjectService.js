import axios from 'axios'
import apiService from "@/service/ApiService";

export default {

    /**
     * Get all queues of the subjects the authenticated STUDENT is associated with.
     *
     * @param token Token which user has authenticated with
     * @returns {Promise<AxiosResponse<any>>} Promise, which will contain the requested data in json
     */
    getStudentSubjectQueues (token) {
        return apiService.getApiClient(token).get("/subjects/queues")
    },

    /**
     * Get all queues of the subjects the authenticated user is associated as a STUDENT_ASSISTANT.
     *
     * @param token Token which user has authenticated with
     * @returns {Promise<AxiosResponse<any>>} Promise, which will contain the requested data in json
     */
    getStudentAssistantSubjectQueues (token) {
        return apiService.getApiClient(token).get("/subjects/queues")
    },

    /**
     * Gets the QueuePositions of the queue associated with the provided subject
     *
     * @param subject Subject to which the queue-positions shall be associated
     * @param token Token which user has authenticated with
     * @returns {Promise<AxiosResponse<any>>}
     */
    getSubjectQueuePositions(subject, token) {
        const subject_code = subject.subjectInfo.subject_code;
        const semester = subject.semester;
        return apiService.getApiClient(token).get(`/subjects/queues/${subject_code}/${semester}/positions`)
    },

    /**
     * Publish new QueuePosition, used by students. User-id associated with token will be added as the queue position-user.
     *
     * @param subject Subject provides hint to queue in which the position shall be appended to
     * @param assignmentNumber Assigment-number which the user is in need of help/approval (-1 if no assignment is chosen)
     * @param queueType Queue-type Help/Approval
     * @param token Token which user has authenticated with
     * @returns {Promise<AxiosResponse<any>>}
     */
    postNewSubjectQueuePosition(subject, assignmentNumber, queueType, token) {
        const subject_code = subject.subjectInfo.subject_code;
        const semester = subject.semester;
        const payload = {
            'assignment_number':assignmentNumber,
            'queue_type':queueType
        }
        return apiService.getApiClient(token).post(`/subjects/queues/${subject_code}/${semester}/positions`, payload)
    },

    /**
     * Get Subject-Object which is assicoated with the provided subject_code and semester.
     *
     * @param subject_code Subject Code which the subject is associated with
     * @param semester Semester which the subject is associated with
     * @param token Token of logged in user.
     * @returns {Promise<AxiosResponse<any>>}
     */
    getSubject(subject_code, semester, token) {
        return apiService.getApiClient(token).get(`/subjects/${subject_code}/${semester}`)
    },

    /**
     * Deletes a queue position from the database
     *
     * @param subject_code
     * @param semester
     * @param student_id
     * @param token
     * @returns {Promise<AxiosResponse<any>>}
     */
    deleteSubjectQueuePosition(subject_code, semester, student_id, token) {
        return apiService.getApiClient(token).delete(`/subjects/queues/${subject_code}/${semester}/positions/${student_id}`)
    },

    /**
     * Update an existing queue position with new data.
     *
     * @param queuePosition
     * @param token
     */
    updateQueuePosition(queuePosition, token) {
        return apiService.getApiClient(token).post("/subjects/queues/positions", queuePosition)
    },

    changeQueueStatus(subject_code, semester, active, token) {
        const json = {
            'active' : active
        }
        return apiService.getApiClient(token).post(`/subjects/queues/${subject_code}/${semester}/status`, json)
    }
}

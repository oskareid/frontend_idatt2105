import axios from 'axios'
import apiService from "@/service/ApiService";

export default {

    /**
     * Gets list of assignments in specified subject with status for the logged-in student.
     *
     * @param subject Subject to which the assignments shall be associated
     * @param token Token which student is authenticated with
     * @returns {Promise<AxiosResponse<any>>}
     */
    getAssignmentsForStudent(subject, token) {

        const subject_code = subject.subjectInfo.subject_code;
        const semester = subject.semester;


        return apiService.getApiClient(token).get(`/subjects/assignments/${subject_code}/${semester}`)
    },

    approveAssignment(subject_code, semester, assignment_number, student_id, token) {
        const json = {
            "assignment_number" : assignment_number,
            "student_id" : student_id
        }

        return apiService.getApiClient(token).post(`/subjects/assignments/${subject_code}/${semester}`, json)
    }
}

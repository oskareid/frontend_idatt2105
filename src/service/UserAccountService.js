import apiService from "@/service/ApiService";

export default {
    /**
     * Gets a string-list of all roles which the current user is associated with.
     *
     * @param token Token which student is authenticated with
     * @returns {Promise<AxiosResponse<any>>}
     */
    getRolesForLoggedInUser(token) {
        return apiService.getApiClient(token).get('/user/roles/');
    },

    /**
     * Gets the user details of the user associated with the given email
     *
     * @param email
     * @returns {Promise<AxiosResponse<any>>}
     */
    getUserAccountDetails(email, token) {
        return apiService.getApiClient(token).get(`/user/${email}`);
    }
}

![Logo](https://i.gyazo.com/a182d8d9c3d2bf85426280c2bba480b9.png)

# Qs3 - NTNU

Frontend to voluntary project - IDATT2105




## Functionality / Usage

- Login using NTNU email

- Student's can queue up for assignment help or approval
- Verified student assistants can pick student to help from queue
- Provides overview of subjects, assignments and important dates
## Documentation

[Backend API](https://gitlab.stud.idi.ntnu.no/oskareid/backend_idatt2104/-/blob/main/README.md)
## Color Reference

| Color             | Hex                                                                |
| ----------------- | ------------------------------------------------------------------ |
| Dark | ![#414141](https://via.placeholder.com/10/414141?text=+) #414141 |
| Green | ![#59d329](https://via.placeholder.com/10/59d329?text=+) #59d329 |
| Red | ![#e33838](https://via.placeholder.com/10/e33838?text=+) #e33838 |
| Contrast white | ![#fdfdfd](https://via.placeholder.com/10/fdfdfd?text=+) #fdfdfd |


## Authors

- [@Oskar Eidem](https://gitlab.stud.idi.ntnu.no/oskareid)
- [@Kristian Aars](https://gitlab.stud.idi.ntnu.no/krisvaa)


## Optimizations

Application follows Web Content Accessibility Guidelines (WCAG) - Firefox dev tools 

- Standardized contrasts

- Text alternatives to supplement color usage and symbols

- Understandable and Predictable

## Further improvements

- Improve error messages

- Postphone assignment

- Assignment feedback to student

## Technologies used

- Vue

- Axios

- Jest

- Vuex

- Vue-Router

## Setup

npm install
## Local Development

npm run serve
## Production

npm run build
## Testing

npm run test:unit
## Acknowledgements

 - [Material-icons API](https://fonts.google.com/icons)
 


## Screenshots

![Subjects-page screenshot](https://i.gyazo.com/61242e3022a6e6b40fb06326db41e772.png)
